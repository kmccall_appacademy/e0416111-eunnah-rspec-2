require "time"

def measure(n = 1, &prc)
  start_time = Time.now
  n.times { prc.call }
  elapsed_time = Time.now - start_time
  average = elapsed_time/n
end
